// Copyright © 2019 Miika Klemetti. All rights reserved.
//
// Use of this source code is governed by a license
// that can be found in the LICENSE file.

package dronex

var (
	header     = [9]byte{0x6c, 0x65, 0x77, 0x65, 0x69, 0x5f, 0x63, 0x6d, 0x64} // "lewei_cmd"
	trailer    = [8]byte{0xe9, 0xaf, 0x2e, 0x5c, 0x00, 0x00, 0x00, 0x00}       // seems random
	commandLen = 46
)

// from controller
// 8060: V1=04 at start
// 7060: V1=02 at start
//       V1=01 (once a sec)

// from drone
// 8060: echo 04 at start
// 7060: 01+01+xx+xx+01 (rapidly)
//       V1=01+v3=40 (once a sec)

// snapshot: v1=13, drone respond v1=13+some payload   over new tcp connection to 8060
// video?:   v1=11 with trailer (20 bytes), drone respond no data, over new tcp connection to 8060

// NOTE: updateMessage seems to occurr in the middle of packets. maybe it indicates the length of the block?

// HEADER-----------------------:V1:V2:-----------------------------:V3:V4:V5:--:V6:--------------------------------------------------------
// heartbeat:
// 6c:65:77:65:69:5f:63:6d:64:00:01:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00
// heartbeat response:
// 6c:65:77:65:69:5f:63:6d:64:00:01:00:00:00:00:00:00:00:00:00:00:00:40:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00
// updateMessage:
// 6c:65:77:65:69:5f:63:6d:64:00:01:01:00:00:00:00:00:00:00:00:00:00:3f:0f:00:00:01:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00
// streamCommand:
// 6c:65:77:65:69:5f:63:6d:64:00:02:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00
// registerCommand:
// 6c:65:77:65:69:5f:63:6d:64:00:04:00:00:00:00:00:00:00:00:00:00:00:08:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:61:79:36:5c:00:00:00:00
// registerResponse:
// 6c:65:77:65:69:5f:63:6d:64:00:04:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00
// takeVideo:
// 6c:65:77:65:69:5f:63:6d:64:00:11:00:00:00:00:00:00:00:00:00:00:00:14:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:10:00:00:00:00:00:00:00:7f:51:01:00:2c:01:00:00
// takeVideo response:
// 6c:65:77:65:69:5f:63:6d:64:00:11:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00
// takePhoto:
// 6c:65:77:65:69:5f:63:6d:64:00:13:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00
// takePhoto response:
// 6c:65:77:65:69:5f:63:6d:64:00:13:00:00:00:00:00:00:00:00:00:00:00:85:9b:02:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00

// embedded message: ? 46+32+0,0,1,21,e0,XX,XX,22,2f,ff = 46+42
// 6c:65:77:65:69:5f:63:6d:64:00:01:00:00:00:00:00:00:00:00:00:00:00:40:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:01:00:00:00:ca:ea:36:5c:00:00:00:00:5d:0f:14:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00

type command struct {
	Header [9]byte  // 9
	_      byte     // 10
	Val1   byte     // 11
	Val2   byte     // 12
	_      [10]byte // 22
	Val3   byte     // 23
	Val4   byte     // 24
	Val5   byte     // 25
	_      byte     // 26
	Val6   byte     // 27
	_      [19]byte // 46
} // 46

var (
	takeListVideosCommand = command{
		Header: header,
		Val1:   0x08, // TODO
	}
	heartbeatCommand = command{
		Header: header,
		Val1:   0x01,
	}
	streamCommand = command{
		Header: header,
		Val1:   0x02,
	}
	takePhotoCommand = command{
		Header: header,
		Val1:   0x13,
	}
	takeVideoCommand = command{
		Header: header,
		Val1:   0x13, // TODO
	}
	calibrateCommand = command{
		Header: header,
		Val1:   0x04,
		Val3:   0x08,
	}
	periodicMessage = command{
		Header: header,
		Val1:   0x01,
		Val2:   0x01,
		Val3:   0x00, // random value?
		Val4:   0x00, // random value?
		Val5:   0x01,
	}
)

func (c command) is(c2 command) bool {
	return c.Val1 == c2.Val1 && c.Val2 == c2.Val2
}

func isCommandResponse(buf []byte) bool {
	return len(buf) >= commandLen &&
		string(buf[:len(header)]) == string(header[:])
}
