// Copyright © 2019 Miika Klemetti. All rights reserved.
//
// Use of this source code is governed by a license
// that can be found in the LICENSE file.

package dronex

import (
	"bytes"
	"context"
	"encoding/binary"
	"fmt"
	"log"
	"net"
)

// Adaptor is a Gobot Adaptor for DroneX quadrocopter.
type Adaptor struct {
	name       string
	remoteAddr string

	streamConn  net.Conn
	controlConn net.Conn

	ctxt      context.Context
	terminate func()
}

// NewAdaptor creates a new instance of Gobot Adaptor for DroneX.
func NewAdaptor(config ...func(*Adaptor)) *Adaptor {
	ctxt, terminate := context.WithCancel(context.Background())
	a := Adaptor{
		name:       "DroneX",
		remoteAddr: defaultRemoteAddr,
		ctxt:       ctxt,
		terminate:  terminate,
	}

	// Call each of the configurator functions.
	for _, fn := range config {
		fn(&a)
	}

	return &a
}

// ConfigLocalhost sets remote address to localhost
func ConfigLocalhost(a *Adaptor) {
	a.remoteAddr = "localhost"
}

// Name implements Gobot Adaptor interface.
func (a *Adaptor) Name() string { return a.name }

// SetName implements Gobot Adaptor interface.
func (a *Adaptor) SetName(name string) { a.name = name }

// Connect implements Gobot Adaptor interface.
func (a *Adaptor) Connect() error {
	var err error

	// Establish connection streaming video and heartbeats.
	a.streamConn, err = a.connectTCP(remoteStreamPort)
	if err != nil {
		return fmt.Errorf("stream %v", err)
	}

	// Establish UDP based control channel that is used for steering.
	a.controlConn, err = a.connectUDP(localControlPort, remoteControlPort)
	if err != nil {
		return fmt.Errorf("control %v", err)
	}
	return nil
}

// Finalize implements Gobot Adaptor interface.
func (a *Adaptor) Finalize() error {
	log.Println("Terminating connection DroneX ...")
	a.terminate()
	if a.controlConn != nil {
		a.controlConn.Close()
	}
	return nil
}

func (a *Adaptor) connectTCP(remotePort string) (net.Conn, error) {
	return net.Dial("tcp", a.remoteAddr+":"+remotePort)
}

func (a *Adaptor) connectUDP(localPort, remotePort string) (net.Conn, error) {
	laddr, err := net.ResolveUDPAddr("udp", "0.0.0.0:"+localPort)
	if err != nil {
		return nil, err
	}
	raddr, err := net.ResolveUDPAddr("udp", a.remoteAddr+":"+remotePort)
	if err != nil {
		return nil, err
	}
	return net.DialUDP("udp", laddr, raddr)
}

func (a *Adaptor) calibrate(responseChan chan<- command) error {
	conn, err := a.connectTCP(remoteCommandPort)
	if err != nil {
		return fmt.Errorf("calibrate %v", err)
	}

	go processConn(a.ctxt, conn, responseChan, nil)

	message := struct {
		Command command
		Trailer [8]byte
	}{
		calibrateCommand,
		trailer,
	}
	return send(conn, message)
}

func (a *Adaptor) takePhoto(
	responseChan chan<- command,
	data *bytes.Buffer,
) error {
	conn, err := a.connectTCP(remoteCommandPort)
	if err != nil {
		return fmt.Errorf("takePhoto %v", err)
	}
	go processConn(a.ctxt, conn, responseChan, data)
	return send(nil, takePhotoCommand)
}

func (a *Adaptor) takeVideo(
	responseChan chan<- command,
	data *bytes.Buffer,
) error {
	conn, err := a.connectTCP(remoteCommandPort)
	if err != nil {
		return fmt.Errorf("takeVideo %v", err)
	}
	go processConn(a.ctxt, conn, responseChan, data)
	return send(conn, takeVideoCommand) // TODO 20B trailer missing
}

func (a *Adaptor) updateControl(c *controlValues) error {
	return send(a.controlConn, c.Message())
}

func (a *Adaptor) sendHeartbeat() error {
	return send(a.streamConn, heartbeatCommand)
}

func (a *Adaptor) startStream(
	responseChan chan command,
	videoStream *bytes.Buffer,
) error {
	// Disable video for now.
	//send(a.streamConn, streamCommand)
	go processConn(a.ctxt, a.streamConn, responseChan, videoStream)
	return nil
}

func send(conn net.Conn, message interface{}) error {
	buffer := new(bytes.Buffer)
	err := binary.Write(buffer, binary.BigEndian, message)
	if err != nil {
		return err
	}
	_, err = conn.Write(buffer.Bytes())
	return err
}
