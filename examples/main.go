// Copyright © 2019 Miika Klemetti. All rights reserved.
//
// Use of this source code is governed by a license
// that can be found in the LICENSE file.

package main

import (
	"log"

	"gobot.io/x/gobot"

	"gitlab.com/mklemetti/dronex"
	"gobot.io/x/gobot/platforms/keyboard"
)

const step = 10

func main() {
	gbot := gobot.NewMaster()

	conn := dronex.NewAdaptor()
	dev := dronex.NewDriver(conn)
	keys := keyboard.NewDriver()

	var speed = 10
	var lateralSpeed, longitudinalSpeed, verticalSpeed, axialSpeed int

	work := func() {
		keys.On(keyboard.Key, func(data interface{}) {
			key := data.(keyboard.KeyEvent)
			switch key.Key {

			case keyboard.One:
				speed = 10
			case keyboard.Two:
				speed = 20
			case keyboard.Three:
				speed = 30
			case keyboard.Four:
				speed = 40
			case keyboard.Five:
				speed = 50
			case keyboard.Six:
				speed = 60
			case keyboard.Seven:
				speed = 70
			case keyboard.Eight:
				speed = 88
			case keyboard.Nine:
				speed = 90
			case keyboard.Zero:
				speed = 100

			case keyboard.Tilde: // Reset controls
				dev.SetLateralSpeed(0)
				dev.SetLongitudinalSpeed(0)
				dev.SetVerticalSpeed(0)
				dev.SetAxialSpeed(0)

			case keyboard.ArrowLeft:
				if lateralSpeed > 0 {
					lateralSpeed = 0
				} else {
					lateralSpeed = -speed
				}
				dev.SetLateralSpeed(lateralSpeed)

			case keyboard.ArrowRight:
				if lateralSpeed < 0 {
					lateralSpeed = 0
				} else {
					lateralSpeed = speed
				}
				dev.SetLateralSpeed(lateralSpeed)

			case keyboard.ArrowDown:
				if longitudinalSpeed > 0 {
					longitudinalSpeed = 0
				} else {
					longitudinalSpeed = -speed
				}
				dev.SetLongitudinalSpeed(longitudinalSpeed)

			case keyboard.ArrowUp:
				if longitudinalSpeed < 0 {
					longitudinalSpeed = 0
				} else {
					longitudinalSpeed = speed
				}
				dev.SetLongitudinalSpeed(longitudinalSpeed)

			case keyboard.A:
				if verticalSpeed < 0 {
					verticalSpeed = 0
				} else {
					verticalSpeed = speed
				}
				dev.SetVerticalSpeed(verticalSpeed)

			case keyboard.Z:
				if verticalSpeed > 0 {
					verticalSpeed = 0
				} else {
					verticalSpeed = -speed
				}
				dev.SetVerticalSpeed(verticalSpeed)

			case keyboard.Q:
				if axialSpeed > 0 {
					axialSpeed = 0
				} else {
					axialSpeed = -speed
				}
				dev.SetAxialSpeed(axialSpeed)

			case keyboard.W:
				if axialSpeed < 0 {
					axialSpeed = 0
				} else {
					axialSpeed = speed
				}
				dev.SetAxialSpeed(axialSpeed)

			case keyboard.S:
				dev.TakeOff()
			case keyboard.X:
				dev.Land()
			case keyboard.D:
				dev.Hover()
			case keyboard.C:
				dev.Halt()
			}
		})

		dev.On(dev.Event(dronex.EvControl), func(data interface{}) {
			log.Println("EvControl:", data)
		})
	}

	robot := gobot.NewRobot(
		"Controller",
		[]gobot.Connection{conn},
		[]gobot.Device{dev, keys},
		work,
	)

	gbot.AddRobot(robot)
	gbot.Start()
}
