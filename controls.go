// Copyright © 2019 Miika Klemetti. All rights reserved.
//
// Use of this source code is governed by a license
// that can be found in the LICENSE file.

package dronex

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"net"
)

var (
	takeoffControl = 0x01
	landControl    = 0x02
	hoverControl   = 0x04
	unknownControl = 0x20
)

type controlValues struct {
	Lateral      int
	Longitudinal int
	Vertical     int
	Axial        int
	Command      int
}

type controlMessage struct {
	Header       byte
	Lateral      byte
	Longitudinal byte
	Vertical     byte
	Axial        byte
	Command      byte
	Checksum     byte
	Trailer      byte
} // 8 B

func (c controlValues) Message() controlMessage {
	m := controlMessage{
		Header:       0x66,
		Lateral:      byte(c.Lateral + 128),
		Longitudinal: byte(c.Longitudinal + 128),
		Vertical:     byte(c.Vertical + 128),
		Axial:        byte(c.Axial + 128),
		Command:      byte(c.Command),
		Trailer:      0x99,
	}
	m.Checksum = m.Lateral ^ m.Longitudinal ^ m.Vertical ^ m.Axial
	return m
}

func (c controlValues) String() string {
	return fmt.Sprintf("Lat:%v, Long:%v, Vert:%v, Axial:%v, Comm:%v",
		c.Lateral,
		c.Longitudinal,
		c.Vertical,
		c.Axial,
		c.Command,
	)
}

func (c controlValues) sendTo(conn net.Conn) error {
	buffer := new(bytes.Buffer)
	binary.Write(buffer, binary.BigEndian, c.Message())
	_, err := conn.Write(buffer.Bytes())
	return err
}
