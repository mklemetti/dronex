# Gobot DroneX

Gobot (http://gobot.io/) is a framework for robotics and physical computing using Go

This repository contains the Gobot adaptor and driver for DroneX Pro quadrocopter
belonging to `Discovery U818A` drone family.

Work in progress. Lots to do.

For more information about Gobot, check out the github repo at
https://gobot.io/x/gobot

## Installing
```bash
go get gitlab.com/mklemetti/dronex
```

## Using
```go
package main

import (
  "fmt"
  "time"

  "gobot.io/x/gobot"
  
  "gitlab.com/mklemetti/dronex"
)

func main() {
  gbot := gobot.NewMaster()

  conn := dronex.NewDronexAdaptor()
  dev := dronex.NewDronexDriver(conn)

  work := func() {
    dev.On(dev.Event(dronex.EvControl), func(data interface{}) {
			log.Println("EvControl:", data)
		})
  }

  robot := gobot.NewRobot(
    "robot",
    []gobot.Connection{conn},
    []gobot.Device{dev},
    work,
  )

  gbot.AddRobot(robot)
  gbot.Start()
}

```

## Connecting

Manually connect to DroneX WiFi, and run `go build -o dronex ./examples; ./dronex`.
Press `s` to takeoff, `x` to land, `a` and `z` for vertical control, and arrow keys for lateral
and longitudinal control.

## License

Copyright (c) 2019 Miika Klemetti. Licensed under the MIT license.
