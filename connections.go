// Copyright © 2019 Miika Klemetti. All rights reserved.
//
// Use of this source code is governed by a license
// that can be found in the LICENSE file.

package dronex

import (
	"bytes"
	"context"
	"encoding/binary"
	"log"
	"net"
)

func processConn(
	ctxt context.Context,
	conn net.Conn,
	responseChan chan<- command,
	data *bytes.Buffer,
) {
	defer conn.Close()
	buf := make([]byte, 2048)

	for {
		select {
		case <-ctxt.Done():
			return

		default:
			n, err := conn.Read(buf)
			if err != nil {
				if ctxt.Err() == nil {
					log.Printf("Read: %v", err)
				}
				return
			}

			// TODO: rework below to 'extractCommandResponse' from the buffer,
			// which returns the command with the remaining bytes.
			// this is required since commands may be embedded within packets in
			// the tcp stream, or even split across packet boundaries.
			// use: bytes.Split(buf[:n], header) + 37 bytes
			if isCommandResponse(buf[:n]) {
				var c command
				err := binary.Read(bytes.NewBuffer(buf), binary.BigEndian, &c)
				if err != nil {
					log.Printf("Parse response: %v", err)
					continue
				}
				responseChan <- c

			} else if data != nil {
				// Copy response buffer to the provided buffer.
				_, err := data.Write(buf[:n])
				if err != nil {
					log.Printf("Copy data: %v", err)
					continue
				}
				log.Printf("Received %v B", n)
			}
		}
	}
}
