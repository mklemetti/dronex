// Copyright © 2019 Miika Klemetti. All rights reserved.
//
// Use of this source code is governed by a license
// that can be found in the LICENSE file.

package dronex

import (
	"log"
	"net"
	"testing"
	"time"

	"gobot.io/x/gobot"
)

var _ gobot.Driver = (*Driver)(nil)

func TestMaster(t *testing.T) {
	startDummyDrone()
	startMaster()
}

func startMaster() {
	gbot := gobot.NewMaster()

	conn := NewAdaptor(ConfigLocalhost)
	dev := NewDriver(conn)

	work := func() {
		dev.On(dev.Event(EvControl), func(data interface{}) {
			log.Println("EvControl:", data)
		})

		gobot.After(200*time.Millisecond, func() {
			dev.TakeOff()
		})

		gobot.After(500*time.Millisecond, func() {
			dev.SetLateralSpeed(10)
		})

		gobot.After(600*time.Millisecond, func() {
			dev.SetLongitudinalSpeed(10)
		})

		gobot.After(700*time.Millisecond, func() {
			dev.SetVerticalSpeed(10)
		})

		gobot.After(800*time.Millisecond, func() {
			dev.SetAxialSpeed(10)
		})

		gobot.After(time.Second, func() {
			dev.Hover()
		})

		gobot.After(2500*time.Millisecond, func() {
			dev.Land()
		})
	}

	robot := gobot.NewRobot(
		"Test",
		[]gobot.Connection{conn},
		[]gobot.Device{dev},
		work,
	)

	gbot.AddRobot(robot)
	go func() {
		time.Sleep(3 * time.Second)
		gbot.Stop()
	}()
	go gbot.Start()

	time.Sleep(5 * time.Second)
}

func startDummyDrone() {
	go listenTCP(remoteCommandPort)
	go listenTCP(remoteStreamPort)
	go listenUDP(remoteControlPort)
}

func listenUDP(port string) {
	addr, err := net.ResolveUDPAddr("udp", "0.0.0.0:"+port)
	if err != nil {
		log.Panic(err)
	}
	l, err := net.ListenUDP("udp", addr)
	if err != nil {
		log.Panic(err)
	}

	for {
		buf := make([]byte, 2048)
		_, err := l.Read(buf)
		if err != nil {
			log.Printf("DummyDrone control: %v", err)
			continue
		}
	}
}

func listenTCP(port string) {
	l, err := net.Listen("tcp", "0.0.0.0:"+port)
	if err != nil {
		log.Panic(err)
	}

	// Accept new connections that keep reading messages
	// and echoing back the base message as confirmations.
	for {
		conn, err := l.Accept()
		if err != nil {
			log.Println("DummyDrone accept:", err)
		}
		log.Printf("DummyDrone accept on %v", conn.LocalAddr())
		go func() {
			defer conn.Close()
			for {
				buf := make([]byte, 2048)
				n, err := conn.Read(buf)
				if err != nil {
					log.Printf("DummyDrone read from %v: %v\n",
						conn.LocalAddr(), err)
					return
				}
				if n < commandLen {
					log.Printf("DummyDrone: read from %v: command len %v B\n",
						conn.LocalAddr(), n)
					return
				}
				//  Echo base message back (minus trailers).
				conn.Write(buf[:commandLen])
			}
		}()
	}
}
