// Copyright © 2019 Miika Klemetti. All rights reserved.
//
// Use of this source code is governed by a license
// that can be found in the LICENSE file.

package dronex

const (
	defaultRemoteAddr = "192.168.0.1"

	remoteStreamPort  = "7060" // TCP
	remoteCommandPort = "8060" // TCP

	remoteControlPort = "50000" // UDP
	localControlPort  = "5010"  // UDP
)
