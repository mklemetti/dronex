// Copyright © 2019 Miika Klemetti. All rights reserved.
//
// Use of this source code is governed by a license
// that can be found in the LICENSE file.

package dronex

import (
	"bytes"
	"context"
	"log"
	"time"

	"gobot.io/x/gobot"
)

// Events
const (
	EvControl = "control"
)

// Driver is a Gobot Driver for DroneX quadrocopter.
type Driver struct {
	name       string
	connection gobot.Connection

	// Current control values to be send to adapter
	// once per interval.
	controlInterval time.Duration
	controlValues   controlValues

	responseChan chan command
	videoStream  *bytes.Buffer

	ctxt      context.Context
	terminate func()

	gobot.Eventer
}

// NewDriver creates a new instance of Gobot Driver for DroneX.
func NewDriver(a *Adaptor) *Driver {
	ctxt, terminate := context.WithCancel(context.Background())

	d := &Driver{
		name:            "DroneX",
		connection:      a,
		controlInterval: 100 * time.Millisecond,
		controlValues:   controlValues{},
		responseChan:    make(chan command, 1),
		videoStream:     new(bytes.Buffer),

		ctxt:      ctxt,
		terminate: terminate,

		Eventer: gobot.NewEventer(),
	}

	d.AddEvent(EvControl)

	return d
}

// Name implements Gobot Driver interface.
func (d *Driver) Name() string { return d.name }

// SetName implements Gobot Driver interface.
func (d *Driver) SetName(name string) { d.name = name }

// Connection implements Gobot Driver interface.
func (d *Driver) Connection() gobot.Connection {
	return d.connection
}

// Start implements Gobot Driver interface.
func (d *Driver) Start() error {
	// Handle responses in the background.
	go d.processResponses()

	// Calibrate drone sensors.
	err := d.adaptor().calibrate(d.responseChan)
	if err != nil {
		return err
	}

	// Start receiving streaming video.
	d.adaptor().startStream(d.responseChan, d.videoStream)

	// Send periodic control and heartbeat messages.
	go d.sendUpdates()

	return nil
}

// Halt implements Gobot Driver interface.
func (d *Driver) Halt() error {
	log.Printf("Terminating device %v ...\n", d.name)
	d.Land()
	time.Sleep(time.Second)
	d.terminate()
	return nil
}

func (d *Driver) adaptor() *Adaptor {
	return d.Connection().(*Adaptor)
}
