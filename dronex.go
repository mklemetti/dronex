// Copyright © 2019 Miika Klemetti. All rights reserved.
//
// Use of this source code is governed by a license
// that can be found in the LICENSE file.

package dronex

import (
	"fmt"
	"log"
	"time"
)

func (d *Driver) TakePhoto() error {
	return d.adaptor().takePhoto(d.responseChan, nil)
}

func (d *Driver) TakeVideo() error {
	return d.adaptor().takeVideo(d.responseChan, nil)
}

func (d *Driver) TakeOff() error {
	d.controlValues.Command = takeoffControl
	go d.timedCommandReset()
	return nil
}

func (d *Driver) Hover() error {
	d.controlValues.Command = hoverControl
	go d.timedCommandReset()
	return nil
}

func (d *Driver) Land() error {
	d.controlValues.Command = landControl
	go d.timedCommandReset()
	return nil
}

func (d *Driver) SetLateralSpeed(value int) error {
	d.controlValues.Lateral = value
	return nil
}

func (d *Driver) SetLongitudinalSpeed(value int) error {
	d.controlValues.Longitudinal = value
	return nil
}

func (d *Driver) SetVerticalSpeed(value int) error {
	d.controlValues.Vertical = value
	return nil
}

func (d *Driver) SetAxialSpeed(value int) error {
	d.controlValues.Axial = value
	return nil
}

func (d *Driver) timedCommandReset() {
	oldVal := d.controlValues.Command
	time.Sleep(time.Second)
	// Reset the
	if d.controlValues.Command == oldVal {
		d.controlValues.Command = 0
	}
}

func (d *Driver) sendUpdates() {
	controlTicker := time.NewTicker(d.controlInterval)
	heartbeatTicker := time.NewTicker(time.Second)
	defer controlTicker.Stop()
	defer heartbeatTicker.Stop()

	for {
		select {
		case <-d.ctxt.Done():
			return

		case <-controlTicker.C:
			// Send latest control values to the drone.
			err := d.adaptor().updateControl(&d.controlValues)
			if err != nil {
				log.Printf("control: %v", err)
				continue
			}

		case <-heartbeatTicker.C:
			err := d.adaptor().sendHeartbeat()
			if err != nil {
				log.Printf("heartbeat: %v", err)
				continue
			}
			// Publish an event declaring control values that were used.
			d.Publish(EvControl, d.controlValues)
		}
	}
}

func (d *Driver) processResponses() {
	for c := range d.responseChan {
		switch {
		case c.is(calibrateCommand):
			// TODO process calibration
			log.Println("Received calibration response")

		case c.is(heartbeatCommand):
			// TODO process hearthbeat
			log.Println("Received heartbeat response")

		case c.is(periodicMessage):
			// TODO process update
			log.Println("Received periodic message")

		default:
			fmt.Printf("unknown response %v\n", c)
		}
	}
}
