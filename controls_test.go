// Copyright © 2019 Miika Klemetti. All rights reserved.
//
// Use of this source code is governed by a license
// that can be found in the LICENSE file.

package dronex

import (
	"log"
	"testing"
)

func TestControlMessage(t *testing.T) {
	c := controlValues{
		Lateral:      128,
		Longitudinal: 0,
		Vertical:     128,
		Axial:        128,
		Command:      1,
	}

	m := c.Message()
	if m.Checksum != 128 {
		log.Println("Bad checksum:", m.Checksum)
		t.FailNow()
	}
}
