// Copyright © 2019 Miika Klemetti. All rights reserved.
//
// Use of this source code is governed by a license
// that can be found in the LICENSE file.

package dronex

import (
	"testing"

	"gobot.io/x/gobot"
)

var _ gobot.Adaptor = (*Adaptor)(nil)

func TestAdaptor(t *testing.T) {
	_ = NewAdaptor()
}
